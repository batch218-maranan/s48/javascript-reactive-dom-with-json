const posts = [];

function addPost(title, body) {
  const newPost = {
    id: posts.length,
    title: title,
    body: body
  };

  posts.push(newPost);

  const postEntriesDiv = document.getElementById("div-post-entries");
  postEntriesDiv.innerHTML += `
    <div>
    <h2>
      ${newPost.title}
    </h2>
    <br>
    <p>
      ${newPost.body}
    </p>
      <button onclick="editPost(${newPost.id})">Edit</button>
      <button onclick="deletePost(${newPost.id})">Delete</button>
    </div>
  `;
//   console.log(addPost)
}

function editPost(id) {
    const post = posts.find(post => post.id === id);
  
    if (post) {
      const editForm = document.getElementById("form-edit-post");
      editForm.hidden = false;
  
      const idInput = document.getElementById("txt-edit-id");
      idInput.value = post.id;
  
      const titleInput = document.getElementById("txt-edit-title");
      titleInput.value = post.title;
  
      const bodyInput = document.getElementById("txt-edit-body");
      bodyInput.value = post.body;
  
      // Add code to update the post when the submit button is clicked
      editForm.addEventListener("submit", e => {
        e.preventDefault();
        post.title = titleInput.value;
        post.body = bodyInput.value;
        
        // Update the post in the DOM
        const postEntriesDiv = document.getElementById("div-post-entries");
        postEntriesDiv.innerHTML = "";
        posts.forEach(post => {
          postEntriesDiv.innerHTML += `
            <div>
            <h2>
              ${post.title}
            </h2>
            <br>
            <p>
              ${post.body}
            </p>
              <button onclick="editPost(${post.id})">Edit</button>
              <button onclick="deletePost(${post.id})">Delete</button>
            </div>
          `;
        });
      });
    }
    console.log(editPost)
  }
  



function deletePost(id) {
  const postIndex = posts.findIndex(post => post.id === id);

  if (postIndex >= 0) {
    posts.splice(postIndex, 1);

    const postEntriesDiv = document.getElementById("div-post-entries");
    postEntriesDiv.innerHTML = "";
    posts.forEach(post => {
      postEntriesDiv.innerHTML += `
        <div>
        <h2>
          ${post.title}
        </h2>
        <br>
        <p>
          ${post.body}
        </p>
          <button onclick="editPost(${post.id})">Edit</button>
          <button onclick="deletePost(${post.id})">Delete</button>
        </div>
      `;
    });
  }
}

const addForm = document.getElementById("form-add-post");
addForm.addEventListener("submit", e => {
  e.preventDefault();
  const titleInput = document.getElementById("txt-title");
  const bodyInput = document.getElementById("txt-body");
  addPost(titleInput.value, bodyInput.value);
});

const editForm = document.getElementById("form-edit-post");
editForm.addEventListener("submit", e => {
  e.preventDefault();
  const idInput = document.getElementById("txt-edit-id");
  const titleInput = document.getElementById("txt-edit-title");
  const bodyInput = document.getElementById("txt-edit-body");
  editPost(parseInt(idInput.value), titleInput.value, bodyInput.value);

});


